import React from 'react';
import './App.css';
import './toDo/ToDoItem/ToDoItem.css';
import ToDoItem from './toDo/ToDoItem/ToDoItem';
import todosData from './toDo/todosData';

function App() {
  const todosItems = todosData.map(item =>{
    return (
      <ToDoItem
        key={item.id}
        description={item.text}
        completed={item.completed}
      />
      
    )
  })
  return (
    <div className="App">
      <div className="cards">
        {todosItems}
      </div>
    </div>
  );
}

export default App;
