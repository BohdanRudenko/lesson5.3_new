import React from 'react';
import './ToDoItem.css';

const ToDoItem = props => {
    return (
        <div className="todo-wrapper">
            <input type="checkbox" defaultChecked={props.completed}/>
            <p className="todo-wrapper__text">{props.description}</p>
        </div>
    )
}

export default ToDoItem