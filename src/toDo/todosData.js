const todosData = [
    {
        id : 1,
        text : "Сходить в магазин",
        completed : true
    },
    {
        id : 2,
        text : "Покормить кота",
        completed : true
    },
    {
        id : 3,
        text : "Сделать ДЗ",
        completed : false
    },
    {
        id : 4,
        text : "Спать!!!",
        completed : false
    },
]

export default todosData